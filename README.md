# Linux Notes

This file contains syntax for useful Linux commands.

## Basics

Change directory (up): `cd ..`

List files in a directory: `ls /my/directory`

Find text in a file: `grep -n "some string" file.txt`

Find text in any file in a directory: `grep -rn "some string" /my/directory`

Delete a directory: `rm -r /my/directory`

Show last lines of file: `tail file.txt`

Show first lines of file: `head file.txt`

## Modifying Files

It can be useful to modify the contents of a file without opening it in an editor. The simplest way to achieve this is using the `sed` command, which can edit files in-place, or edit data from streams. The following command replaces `old string` with `new string` in `file.txt`

`sed -i 's/old string/new string/g' file.txt`

You may also want to replace strings throughout all files in a directory and its subdirectories. This can be achieved using `sed` with the `find` and `xargs` commands as follows;

`find . -type f | xargs sed -i 's/text/te xt/g'`

In plain English this reads as; find everything starting in the current directory (`.`), which is of type file (`-f`). For each file we find, execute the `sed` command as above, and append the file (from the `|`) to the end of the command. For details see `man find`, `man xargs`, and `man sed`.


## File Transfer

### FTP Servers

Access FTP servers using the `lftp` command. If they are credentialed, prepend the credentials to the server name, separated by a colon. Note that this is a security risk as it exposes the credentials to anyone on the same terminal.

`lftp username:password@myftp.website.com`

### Copying Remote Files

To retrieve files from a remote host, use the `scp` command. The high level syntax is `scp from to`, and can either be used to 'push' a file from the local machine to the remote machine, or to 'pull' a file from the remote machine to the local machine.

`scp /some/local/file.txt oliver@remote_host.com:/remote/directory`

### Getting Data from Endpoints

Endpoints like websites, FTP servers, and API's, return data following requests using a specific protocol/format. The simplest way to send such requests in Linux is using the [`curl`](https://everything.curl.dev/) program, which is a command line utility for transferring data with URL syntax.

Its basic usage is simply the command followed by the resource to retrieve;

`curl www.some_website.com`

You can also use `curl` to access FTP sites, although I prefer the `lftp` command described above as it's more obvious what it's doing.

### Cloud Storage

Cloud storage providers offer a way to store data in a distributed and secure way. Two commonly used cloud providers are Amazon Web Services (AWS) and Google Cloud Storage (GCS).

**AWS**

AWS has a command line interface which has basic usage similar to the basic Linux commands at the top of this file. Many commands simply work as expected following the `aws` command.

`aws s3 ls` # list all buckets owned by the user

`aws s3 ls s3://my_bucket` # list all files in a buckets

`aws s3 cp s3://my_bucket/file.txt local_file.txt` # copy file in bucket to local machine (like `scp` above)

**GCS**

GCS also has a command line interface with similar syntax.

`gsutil ls gs://my_bucket/**` # list files in a bucket

`gsutil cp local_file.txt gs://my_bucket/file.txt` # copy local file to gcs bucket

## Encryption

Storing credentials in plaintext is a security risk as they can be read by anyone with access to the computer. Encrypting (and decrypting) these files protects them from other users on the machine, and allows open storage and transmission in their encrypted form.

To encrypt a file in Linux, the `gpg` tool can be used, which provides encryption, decryption, signing, and verification capabilites. Example;

`gpg --symmetric file.txt`

This will let the file be encrypted and decrypted using the same passphrase (see [Symmetric-key Algorithm](https://en.wikipedia.org/wiki/Symmetric-key_algorithm)). To use asymmetric encryption, you will need to generate a key pair using;

`gpg --full-generate-key`

This will create a key pair which you can use to encrypt and decrypt files;

`gpg --encrypt -r recipient@email.com file.txt` # produces file.txt.gpc

`gpg --decrypt file.txt.gpg > file.txt`

## Compression

Compressing files can lead to dramatic reductions in their storage size, which is useful for transferring over low-bandwidth networks and long term storage. A number of compression/archiving utilities are usually available in Linux, one common one is `tar`. To compress a single file using tar, the following command can be used;

`tar -cz file.txt -f output.tar`

To create a compressed archive of an entire directory, the `tar` command can be used again, but provide the directory you want to compress instead;

`tar -cz /my/directory -f compressed_directory.tar`

This command is actually using the `tar` program to create a single file from multiple files/directories (an archive), and is then compressing that archive using `gzip`. You can use compressing utilities like `gzip`, `bzip2`, `zip`, and more
