# TMUX

TMUX is a terminal multiplexing tool which is a good first step towards a
mouse-free life. To get started, run `tmux`.

You can enter commands to tmux, from tmux, by pressing Ctrl-b (written as `C-b`) followed by a
command. E.g. `C-b &` to kill the current window.

Here are the main useful commands I've found so far;

```
C-b c = create a new window
C-b n = go to the next window
C-b l = go to the last (previous) window
C-b % = split the current pane horizontally
C-b " = split the current pane vertically
C-b <SPACE> = arrange the panes in the current window to the next standard
layout
C-b x = kill the current pane
C-b & = kill the current window
C-b ? = show a list of all available commands
```

